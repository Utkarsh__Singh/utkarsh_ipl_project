<!-- Project init -->
* [X] ~~*download CSV files from https://www.kaggle.com/manasgarg/ipl*~~ [16-11-2021]

* [X] ~~*Create a new repo with name <your_first_name>_IPL_Project in Gitlab, before starting implementation of the solution*~~ [16-11-2021]

* [X] ~~*implementation of Directory Structure*~~ [16-11-2021]

**Todo => Implement the `4 functions, one for each task`. Use the results of the functions to dump `JSON files in the output folder`.**
* [X] ~~*Number of matches played per year for all the years in IPL.*~~ [17-11-2021]

* [X] ~~*Number of matches won per team per year in IPL.*~~ [18-11-2021]

* [X] ~~*Extra runs conceded per team in the year 2016*~~ [23-11-2021]

* [X] ~~*Top 10 economical bowlers in the year 2015*~~ [24-11-2021]

* [X] ~~*Find the number of times each team won the toss and also won the match*~~ [24-11-2021]

* [X] ~~*Find a player who has won the highest number of Player of the Match awards for each season*~~ [24-11-2021]
----

## Before submission, make sure that all the points in the *below checklist are covered:

* Git commits
* Directory structure
* package.json - `dependencies`, `devDependencies`
* .gitignore file
* Proper/Intuitive Variable names
* Separate module for functions
